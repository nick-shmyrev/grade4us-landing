# grade4us
A minimalistic landing page for college project. Built using [Material Design theme for Bootstrap 4](https://mdbootstrap.com/)

## See it in action
**[Click.](https://htmlpreview.github.io/?https://github.com/Nick-Shmyrev/grade4us/blob/master/index.html)**

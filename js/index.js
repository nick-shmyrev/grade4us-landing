(function() {
  "use strict";

  const body = document.querySelector('body');
  const imageArray = [
    './img/paint-notebook-brush-pencil-159657.jpg',
    './img/pexels-photo-269481.jpg',
    './img/books-library-knowledge-tunnel-50548.jpg',
    './img/pexels-photo-236110.jpg',
  ];
  const arrayLength = imageArray.length;
  let index = 0;

  // Preload images to avoid img flickering on bg change
  imageArray.forEach((img) => {
    const image = new Image();
    image.src = img;
  });

  setInterval(() => {
    body.style.backgroundImage = `url('${imageArray[index]}')`;
    index = (index + 1) % arrayLength;
  }, 5000);
})();